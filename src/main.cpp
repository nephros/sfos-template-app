// Copyright 2022 Peter G. <sailfish@nephros.org>
// SPDX-FileCopyrightText: 2025 Peter G. (nephros)
//
// SPDX-License-Identifier: Apache-2.0

/*
#include <QQuickView>
#include <QScopedPointer>
#include <QtQuick>
*/
#include <sailfishapp.h>

int main(int argc, char **argv) {
    return SailfishApp::main(argc, argv);
}
