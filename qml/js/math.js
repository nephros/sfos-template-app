/*
 * This file is part of AppTemplate
 * Copyright (c) 2025 Peter G. (nephros)
 * SPDX-License-Identifier: Apache-2.0
 */
.pragma library

function getFraction(n1, n2) {
    // Calculate the greatest common divisor (GCD) of the numerator and denominator
    function gcd(a, b) {
        while (b !== 0) {
            var temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
    var g = gcd(Math.abs(n1), Math.abs(n2));
    var num = n1 / g;
    var denom = n2 / g;
    return "" + num + ":" + denom;
}
// vim: ft=javascript expandtab ts=4 sw=4 st=4 syntax=qml
