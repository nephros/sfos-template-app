#!/bin/sh

printf "This script is intended for building on-device only.\nUse the usual '(auto|c|q)make' build systems if using SDK, OBS etc.\n\n"
specify -Nns rpm/*yaml || exit 1
printf linting...
find qml/ -type f -name "*.qml" -exec qmllint {} +
[ -e python ] && find python/ -type f -name "*.py" -exec python3 -m py_compile {} \;
[ -e qml/python ] && find qml/python/ -type f -name "*.py" -exec python3 -m py_compile {} \;
printf building...
rpmbuild -bb --build-in-place -D "_sourcedir $PWD/rpm" rpm/*.spec > build.log 2>&1
#rpmbuild -bb --build-in-place rpm/*.spec > build.log 2>&1
ex=$?
if [ $ex -eq 0 ]; then
  out=$(awk '/^Wrote/ {print $2}' build.log)
  if [ -e "$out" ]; then
    if [ -x /usr/libexec/sdk-harbour-rpmvalidator/rpmvalidation.sh ]; then
        /usr/libexec/sdk-harbour-rpmvalidator/rpmvalidation.sh -c -d 0 $out | tee validation.log
    else
        printf 'Validation skipped. To enable do\n"%s %s"\n' "pkcon install" "sdk-harbour-rpmvalidator"
    fi
  printf 'To install do either:\npkcon install-local %s\nxdg-open %s\n' $out $out
  fi
  make -s distclean
fi
printf "exit: $ex\n"
