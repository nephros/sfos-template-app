# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.34.2
# 

Name:       pyproject

# >> macros
# << macros
%define srcname %{name}

Summary:    Python AppTemplate Port Extension
Version:    0.0
Release:    0
Group:      Development/Libraries
License:    BSD
BuildArch:  noarch
URL:        https://pypi.org/pytemplate/pytemplate
Source0:    %{pypi_source} %name
Source1:    %{pypi_source} %srcname
Source100:  pyproject.yaml
Source101:  pyproject-rpmlintrc
BuildRequires:  pkgconfig(python3)
BuildRequires:  python3-base
BuildRequires:  pkgconfig
BuildRequires:  pyproject-rpm-macros
BuildRequires:  python3dist(pip)
BuildRequires:  python3dist(toml)

%description
%{summary}

%if "%{?vendor}" == "chum"
PackagedBy: nephros
Categories:
 - Library
Custom:
  Repo: https://github.com/template-app/pytemplate-app
Links:
  Homepage: %{url}
%endif


%package -n python3-%{name}
Summary:    %{summary}
Group:      Development/Libraries

%description -n python3-%{name}
%{summary}.

%define spectacle_hack_starts here
%generate_buildrequires
%define spectacle_hack_ends here

%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
%pyproject_wheel
# << build pre



# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
%pyproject_install
# << install pre

# >> install post
# fix shebangs
find %{buildroot}/%{python3_sitelib} -type f -exec sed -i '1s=^#!/usr/bin/\(python\|env python\)[23]\?=#!%{__python3}=' {} +

# generate some icons«
for size in 86 108 128 172 256 512 1024; do
install -d %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps/
sailfish_svg2png -z 1.0 -f rgba -s 1 1 1 1 1 1 1 %{_datadir}/icons/hicolor/scalable/apps/ %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps/
done«
# << install post


%files -n python3-%{name}
%defattr(-,root,root,-)
%{python3_sitelib}/%{srcname}-*-info
%{python3_sitelib}/%{srcname}/
# >> files python3-%{name}
# << files python3-%{name}
