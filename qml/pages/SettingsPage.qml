/*
 * This file is part of AppTemplate.
 *
 * Copyright (c) 2025 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */
import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page { id: settingsPage

    allowedOrientations: Orientation.All

    onStatusChanged: (status === PageStatus.Deactivating) ? app.refresh() : 0

    /*
    * call to open a Jolla Settings entry:
    *

    DBusInterface { id: jollasettings
        service: "com.jolla.settings"
        path: "/com/jolla/settings/ui"
        iface: "com.jolla.settings.ui"
        function open(page) {
            call ("showPage", [ page ], function(){},function(){})
        }
    }

    ValueButton {
        label: qsTr("Top Menu Settings")
        description: qsTr("")
        onClicked: { settings.open("system_settings/look_and_feel/topmenu") }
    }
    */

    /*
    * call to open a Jolla Settings entry:
    *
    DBusInterface {
        id: dbus
        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        propertiesEnabled: true

        // start handling signals only after the first run:
        // signalsEnabled: false
        // Component.onCompleted: call('Subscribe'); // get signals
        function enable(u) {
            //console.debug("dbus enable unit", u );
            typedCall('EnableUnitFiles', [
                { "type": "as", "value": [u] },
                { "type": "b", "value": false },
                { "type": "b", "value": false },
            ],
                function(result) { successMsg(qsTr("Enable %1").arg(u), result[1]); app.refresh() },
                function(result) { failMsg(qsTr("Enable %1").arg(u), result[1]) }
            );
        }
        function disable(u) {
            //console.debug("dbus disable unit", u );
            typedCall('DisableUnitFiles', [
                { "type": "as", "value": [u] },
                { "type": "b", "value": false },
            ],
                function(result) { successMsg(qsTr("Disable %1").arg(u), result); app.refresh() },
                function(result) { failMsg(qsTr("Disable %1").arg(u), result) }
            );
        }
        function start(u) {
            //console.debug("dbus start unit", u );
            call('StartUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); watchJob(result, qsTr("Start")); },
                function(result) { failMsg(qsTr("Start %1").arg(u), result) }
            );
        }
        function stop(u) {
            //console.debug("dbus stop unit", u );
            call('StopUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); watchJob(result, qsTr("Stop")); },
                function(result) { failMsg(qsTr("Stop %1").arg(u), result) }
            );
        }
        function restart(u) {
            //console.debug("dbus restart unit", u );
            call('RestartUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); watchJob(result, qsTr("Restart")); },
                function(result) { failMsg(qsTr("Restart %1").arg(u), result) }
            );
        }
    }
    */

    SilicaFlickable{
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            spacing: Theme.paddingSmall
            bottomPadding: Theme.itemSizeLarge
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            PageHeader{ title: qsTr("Settings", "page title")}
            SectionHeader { text: qsTr("Application") }
            TextSwitch{ id: notifysw
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: appConfig.notify
                automaticCheck: true
                text: qsTr("Show Notifications")
                description: qsTr("If enabled, the app will send notifications.<br /> Use the slider below to specify how often to check.")
                onClicked: appConfig.notify = checked
            }
            Slider {
                id: intervalSlider
                enabled: notifysw.checked
                handleVisible: enabled
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                //label: qsTr("Interval in %1","parameter is minutes or seconds").arg(qsTr("minutes"));
                label: qsTr("Check every");
                minimumValue: 1
                maximumValue: 120
                stepSize: 1
                value: appConfig.checkInterval / 5
                //valueText: value * 5
                valueText: Format.formatDuration(value*5,Formatter.DurationShort) + " " + qsTr("min");
                onReleased: appConfig.checkInterval = sliderValue * 5
            }
           TextSwitch{
                enabled: notifysw.checked
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: appConfig.notifySticky
                automaticCheck: true
                text: qsTr("Sticky Notifications")
                description: qsTr("If enabled, the app will update a single notification (as opposed to sending a new one each time).")
                onClicked: appConfig.notifySticky = checked
            }

            SectionHeader { text: qsTr("Advanced:")}
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: !appConfig.showFoo
                automaticCheck: true
                text: qsTr("Hide Foo")
                description: qsTr("If enabled, the app will ...")
                onClicked: appConfig.hybris = !checked
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: appConfig.showBar
                automaticCheck: true
                text: qsTr("Show bar")
                description: qsTr("If enabled, the app will ...")
                onClicked: appConfig.boring = !checked
            }
        }
        /*
        PullDownMenu {
            //MenuLabel { text: qsTr("Settings") }
            MenuItem {
                text: qsTr("Reset all to default")
                onClicked: { Remorse.popupAction(settingsPage, qsTr("All settings cleared"), function() { appConfig.resetSettings() } )}
            }
        }
        PushUpMenu {
            visible: (appConfig.pignore.count > 0 || appConfig.ignore.count > 0)
            MenuItem {
                enabled: pList.count > 0
                text: qsTr("Clear permanent ignore list")
                onClicked: { Remorse.popupAction(settingsPage, qsTr("List Cleared."), function() { appConfig.resetIgnorePerm() } )}
            }
            MenuItem {
                enabled: iList.count > 0
                text: qsTr("Clear session ignore list")
                onClicked: { Remorse.popupAction(settingsPage, qsTr("List Cleared."), function() { appConfig.ignore.clear(); } )}
            }
        }
        */
        VerticalScrollDecorator {}
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4 syntax=qml
