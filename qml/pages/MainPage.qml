/*
 * This file is part of AppTemplate.
 *
 * Copyright (c) 2025 Peter G. (nephros)
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

import QtQuick 2.6
import Sailfish.Silica 1.0
//import "../components"

Page { id: page

    allowedOrientations: Orientation.All

    /*
     * alternative: ColumnView instead of Flickable + Column
     */
    /*
    ColumnView { id: view
        anchors.fill: parent
        add:      Transition { FadeAnimation { duration: 1200 } }
        move:     Transition { FadeAnimation { duration: 1200 } }
        populate: Transition { FadeAnimation { duration: 1200 } }
        clip: true
        pullDownMenu: pdp
        pushUpMenu: pup
        model: listModel
        delegate: SilicaItem { }
    }
    */

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingLarge
            bottomPadding: Theme.itemSizeLarge
            add:      Transition { FadeAnimation { duration: 1200 } }
            move:     Transition { FadeAnimation { duration: 1200 } }
            populate: Transition { FadeAnimation { duration: 1200 } }
            PageHeader { id: head ; title: qsTr("Page Header") }
        }
        PullDownMenu { id: pdp
            MenuItem { text: qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
            MenuItem { text: qsTr("Settings"); onClicked: { pageStack.push(Qt.resolvedUrl("SettingsPage.qml")) } }
        }
        /*
    PushUpMenu { id: pup
        quickSelect: false
        MenuLabel { text: qsTr("Experimental or dangerous actions") }
        MenuItem  { text: qsTr(""); onClicked: { } }
    }
    */

        RemorsePopup {
            id: applyRemorse
        }

        VerticalScrollDecorator {}
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript syntax=qml
