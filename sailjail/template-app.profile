# -*- mode: sh -*-

# Firejail profile for template-app

# x-sailjail-translation-catalog = sailjail-permissions
# x-sailjail-translation-key-description = permissions-desc
# x-sailjail-description = Permissions for template-app
# x-sailjail-translation-key-long-description = permissions-long_description
# x-sailjail-long-description = Permissions for template-app

## PERMISSIONS
# x-sailjail-permission = MediaIndexing
# x-sailjail-permission = Pictures
# x-sailjail-permission = Videos

# if we provide a profile, and are QML only we need to whitelist this:
private-bin /usr/bin/sailfish-qml

