// Copyright (c) 2025 Peter G. (nephros)
// SPDX-License-Identifier: Apache-2.0

QtObject { id: root
    property string versionString: ""
    property int maj: 0; property int min: 0; property int rel: 0; property int patch: 0;
    onVersionStringChanged: if (versionString.length) console.info("Detected Sailfish OS version" + versionString)
    Component.onCompleted: {
        var fileUrl = "file:///etc/sailfish-release"

        var r = new XMLHttpRequest()
        r.open('GET', fileUrl);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.send();

        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                const vre = /^VERSION_ID=([0-9.]*)$/gm;
                const v = vre.exec(r.response)[1];
                const va = v.split(".")
                root.versionString = v;
                root.maj =   va[0];
                root.min =   va[1];
                root.rel =   va[2];
                root.patch = va[3];
            }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4 syntax=qml
