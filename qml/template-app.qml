/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (c) 2025 Peter G. (nephros)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
//import Nemo.Mce 1.0      // power saving mode
//import Nemo.Connectivity 1.0 // online/offline detection
//import Nemo.DBus 2.0
// better not to import all the pages at launch...
import "pages"
import "cover"

// Settings in Database instead of DConf:
//import "utils"
//import "js/SettingsDatabase.js" as SettingsDatabase

ApplicationWindow {
    id: app

    allowedOrientations: Orientation.All

    //property alias online: onlineHelper.online
    //property bool offline: !online

    //property alias powersaving: powerSaveMode.active


    /* detect closing of app*/
    /*
    signal willQuit()
    Connections { target: __quickWindow; onClosing: willQuit() }
    // AND/OR
    Connections { target: Qt.application; onAboutToQuit: willQuit() }
    onWillQuit: {
        if (_willquit Handled) return
        //... do scomething
        _willquitHandled=true
    }
    property bool _willquitHandled: false
    */

    /*
    background.wallpaper: Component { id: wpImage
        Image {
            // silence some warnings
            property url imageUrl: source
            property string wallpaperFilter: ""
            source: Qt.resolvedUrl("http://i.stack.imgur.com/NO8hx.png")
            asynchronous: true
            fillMode: Image.PreserveAspectFit
        }
    }
    */

    /*
    // Application goes into background or returns to active focus again
    onApplicationActiveChanged: {
        if (applicationActive) {
        } else {
        }
    }
    */

    /* detect Os Version */
    //OsVersion { id: osVersion }

    /*
     McePowerSaveMode { id: powerSaveMode }
    */

    /* Settings in database not in DConf: */
    /*
    Settings { id: settings
        Component.onCompleted: {
            SettingsDatabase.load();
            SettingsDatabase.transaction(function(tx) {
                    var mySetting = SettingsDatabase.transactionGet(tx, "mySetting");
                    settings.mySetting = (mySetting === false ? "some-default-value" : mySetting);
            }
        }
    }
    */


    Component.onCompleted: {
        // for sailjail
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Initialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
        /* 
         * this stopped working at some point as the key does not exist any more
         *
        // correct landscape for Gemini, set once on start
        allowedOrientations = (devicemodel === 'planetgemini')
            ? Orientation.LandscapeInverted 
            : defaultAllowedOrientations
            */
    }

    /*
    // correct landscape for Gemini
    ConfigurationValue {
        id: devicemodel
        key: "/desktop/lipstick-jolla-home/model"
    }
    */

    // application settings:
    property alias appConfig: appConfig
    ConfigurationGroup  {
        id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup  {
        id: appConfig
        scope: settings
        path:  "app"
        //property bool gravity: true // true: pull to bottom
        //property int ordering: 1 // 1: top to bottom
    }

    /*
     * Dbus listener for openUrl/openApp etc
    */
    /*
    // pointless assignment for debugging with qmlscene, which calls itself "QtQmlViewer":
    readonly property string busname: (Qt.application.name === "QtQmlViewer") ? "SailVP" : Qt.application.name
    DBusAdaptor { id: listener
        bus: DBus.SessionBus
        service: "org.nephros.sailfish." + busname
        iface:   "org.nephros.sailfish." + busname
        path:    "/org/nephros/sailfish/" + busname
        xml:  '<interface name="' + iface + '">\n'
            + '<method name="open" />\n'
            + '<method name="openPage">\n'
            + '  <arg name="page" type="s" direction="in">\n'
            //+ '    <doc:doc><doc:summary>Name of the page to open</doc:summary></doc:doc>\n'
            + '  </arg>\n'
            + '</method>\n'
            + '<method name="openUrl">\n'
            + '  <arg name="url" type="s" direction="in">\n'
            //+ '    <doc:doc><doc:summary>URL of the page to open</doc:summary></doc:doc>\n'
            + '  </arg>\n'
            + '</method>\n'
            + '<method name="trigger" />\n'
            //+ '<method name="login" />\n'
            //+ '<method name="logout" />\n'
            + '<property name="ready" type="b" access="read" />\n'
            + '<property name="unread" type="ai" access="read" />\n'
            + '</interface>\n'

        property bool ready: (!powerSaveMode.active && APIItem.loggedin) ? true : false
        property var unread: [ APIItem.notificationCount, APIItem.chatCount, APIItem.newEventCount ]
        function login()  { app.warn("DBus login() called, not implemented.") }
        function logout() { app.warn("DBus logout() called, not implemented.") }
        function activate() { open() }
        function open() {
            console.info("App opened via DBus call.")
            __silica_applicationwindow_instance.activate()
        }

        Component.onCompleted: console.debug(qsTr("DBus service %1 ready").arg(service))
        }
     */



    initialPage: Component { MainPage{} }
    cover: CoverPage{}

    //PageBusyIndicator { running: app.status === Component.Loading }

    /*
    ConnectionHelper { id: onlineHelper }
    */

    /*
    // show popups outside of application
    // !! Note !! needs "privileged" permissions!
    DBusInterface {
        id: windowPrompt

        service: "com.jolla.windowprompt"
        path: "/com/jolla/windowprompt"
        iface: "com.jolla.windowprompt"

        function showConnectionError() {
            var title = qsTr("Connection failed")
            var body = qsTr("Could not establish connection to host %1).arg(host)
            windowPrompt.call("showInfoWindow", [title, "", body])
        }

    }
    */
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4 syntax=qml
