/*
 * This file is part of AppTemplate.
 * Copyright (c) 2025 Peter G. (nephros)
 * SPDX-License-Identifier: Apache-2.0
 */
.pragma library


function tls(n) {
    return Number(n).toLocaleString(Qt.locale(), 'f', 0)
}

function uppercaseArray(a) {
    return a.map(function(word){
        return word[0].toUpperCase() + word.substring(1)
    }).join(" ")
}

function upperCaseString(s) {
    return uppercaseArray(s.split(" "))
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4 syntax=qml
