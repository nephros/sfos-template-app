<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="32"/>
        <source>What&apos;s %1?</source>
        <translation>Was ist %1?</translation>
    </message>
    <message>
        <source> </source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <source>%1 is ..</source>
        <translation type="vanished">%! ist ein ...</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="18"/>
        <source>For detailed licensing information on all application content see the file REUSE.toml in the source repository.</source>
        <translation>Detaillierte Informationen zur Lizensierung aller Kompoenten der Application die Datei REUSE.toml im Quellcodeverzeichnis.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Copyright:</source>
        <translation>Urheberrecht:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <source>Source Code:</source>
        <translation>Quellcode:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Credits</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="54"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>%1 Übersetzung</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="48"/>
        <source>Page Header</source>
        <translation>Seitenkopf</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="51"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="52"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="110"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="111"/>
        <source>Application</source>
        <translation>Applikation</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="117"/>
        <source>Show Notifications</source>
        <translation>Benachrichtigungen anzeigen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="118"/>
        <source>If enabled, the app will send notifications.&lt;br /&gt; Use the slider below to specify how often to check.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="128"/>
        <source>Check every</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="134"/>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="143"/>
        <source>Sticky Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="144"/>
        <source>If enabled, the app will update a single notification (as opposed to sending a new one each time).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="148"/>
        <source>Advanced:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="154"/>
        <source>Hide Foo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="155"/>
        <location filename="../qml/pages/SettingsPage.qml" line="164"/>
        <source>If enabled, the app will ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="163"/>
        <source>Show bar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
