// Copyright (c) 2025 Peter G. (nephros)
//
// SPDX-License-Identifier: Apache-2.0

//pragma Singleton
import QtQuick 2.6
import Sailfish.Silica 1.0

/*! Wrapper around QML::XMLHttpRequest()
*/
QtObject { id: obj

    readonly property string userAgent: "Mozilla/5.0 (Sailfish OS; Linux; aarch64; rv:92.0) Gecko/20100101 Firefox/92.0 " + Qt.application.name + "/" + Qt.application.version //!< User Agent to use for all requests
    readonly property string apiHost:   "https://api.example.org" //!< The server to use for all requests
    readonly property string apiUrl:    apiHost + "/v1" //!< prefix to any endpoints given in ::xhr, ::xhrpost, or ::xhrbin

    /*! Authentication token.

       MUST be set before issuing any requests.
       Will be the value of an 'Authorization' request-header.
    */
    property string token: ""

    //! \internal
    property var headers: []

    readonly property string dlRoot: StandardPaths.download //! Prefix used to construct ::dlPath. Default: Sailfish.Silica:#StandardPaths.download
    readonly property string dlPath: dlRoot + "/" + Qt.application.name //! where to download binary files

    /*! Adds a custom header for requests.

       May be used to set additional request-headers to for any requests.
       @param type:Object header Format: `{ "header": headerName, "value": value }`
    */
    function addHeader(header) {
        var h = headers
        h.push(header)
        headers = h
    }

    /*! Executes an HTTP Request
       @param type:string   url endpoint
       @param type:string   type HTTP method, e.g. "GET"
       @param type:bool     partial whether to accept partial responses
       @param type:function callback function to call on the result text, as JSON
    */
    function xhr(url, type, partial, callback) {
        if (token === "") return false;   // not without token!
        var query = Qt.resolvedUrl(url);
        var r = new XMLHttpRequest();
        r.open(type, query);
        r.setRequestHeader('User-Agent', userAgent)
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.setRequestHeader('Authorization', token);
        //r.setRequestHeader('X-Auth-Token', token);
        //r.setRequestHeader('X-App-Client', Qt.application.name);
        //r.setRequestHeader('X-App-Version', Qt.application.version);
        r.setRequestHeader('Accept', 'application/json');
        r.setRequestHeader('Origin', '');

        r.send();
        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                if (partial && r.status === 206) {
                    var rdata = JSON.parse(r.response);
                    callback(rdata)
                } else if (r.status === 200 || r.status == 0) {
                    var rdata = JSON.parse(r.response);
                    callback(rdata)
                } else {
                    console.debug("error in processing request.", query, r.status, r.statusText);
                    obj.lastError = r.statusText;
                }
            busy = false;
            }
        }
    }

    /*! Executes an HTTP Request including a payload
       @param type:string   url endpoint
       @param type:string   type HTTP method, typically "POST"
       @param type:string   payload the content to the post request
       @param type:function callback function to call on the result text, as JSON
    */
    function xhrpost(url, type, payload, callback) {
        if (token === "") return false;   // not without token!
        var query = Qt.resolvedUrl(url);
        var r = new XMLHttpRequest();
        r.open(type, query);
        r.setRequestHeader('User-Agent', userAgent)
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.setRequestHeader('Authorization', token);
        //r.setRequestHeader('X-Auth-Token', token);
        //r.setRequestHeader('X-App-Client', Qt.application.name);
        //r.setRequestHeader('X-App-Version', Qt.application.version);
        r.setRequestHeader('Accept', 'application/json');
        r.setRequestHeader('Origin', '');
        r.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

        console.debug("posting:", payload);

        r.send(payload);

        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                if (r.status === 200 || r.status == 0) {
                    var rdata = JSON.parse(r.response);
                    callback(rdata)
                } else {
                    console.debug("error in processing request.", r.status, r.statusText);
                    obj.lastError = r.statusText;
                }
            busy = false;
            }
        }
    }

    /*! Executes an HTTP GET Request returning binary content

       The binary result will be written to a file called \c name, in a directory defined by XHRItem::dlPath
       @param type:string   url endpoint
       @param type:string   name filename (not path) the content should be written to.
    */
    function xhrbin(url, name) {
        if (token === "") return false;   // not without token!
        var query = Qt.resolvedUrl(url);
        var r = new XMLHttpRequest();
        r.open('GET', query);
        r.responseType = 'arraybuffer';
        r.setRequestHeader('User-Agent', userAgent)
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.setRequestHeader('Authorization', token);
        //r.setRequestHeader('X-Auth-Token', token);
        //r.setRequestHeader('X-App-Client', Qt.application.name);
        //r.setRequestHeader('X-App-Version', Qt.application.version);
        r.setRequestHeader('Origin', '');

        r.send();
        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                if (r.status === 200 || r.status == 0) {
                    console.debug("OK, data received.", r.status, r.statusText, r.getResponseHeader("mime-type"));
                    var tmp = sa.writeContentToFile(
                         { "name": name, "type": r.getResponseHeader("mime-type"), "data": r.response }
                    )
                    console.debug("OK, file written.", tmp);
                    FileEngine.rename(tmp, dlPath + "/" + name, true);
                    lastDownloadedFile = dlPath + "/" + name;
                    console.debug("OK, file copied.", lastDownloadedFile);
                } else {
                    console.debug("error in processing request.", r.status, r.statusText);
                    obj.lastError = r.statusText;
                }
            busy = false;
            }
        }
    }
}
// vim: ft=javascript expandtab ts=4 sw=4 st=4 syntax=qml
