#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ]; then
  printf 'USAGE: %s harbour-appname prettyname\n' ${0:t}
  exit 0
fi

find * -type f -name "template-app.*" | xargs rename template-app. "$1".
find * -name "template-app-*" | xargs rename template-app- "$1"-

# Note: use * not ., we do not want to mangle stuff in .git
grep -lr template-app * | grep -v rename.sh | xargs sed -i "s/template-app/$2/g"
grep -lr AppTemplate  * | grep -v rename.sh | xargs "s/AppTemplate/$2/g"
printf "Potential leftovers:"
grep -ir template *
