#!/bin/sh

# see: https://legacy.imagemagick.org/Usage/thumbnails/#rounded

convert -resize 1024x1024 -extent 1200x1200 -gravity center -background "rgb(255,255,255)" $1 /tmp/tmp.png
convert /tmp/tmp.png \
     \( +clone  -alpha extract \
        -draw 'fill black polygon 0,0 0,300 300,0 fill white circle 300,300 300,0' \
        \( +clone -flip -flop \) -compose Multiply -composite \
     \) -alpha off -compose CopyOpacity -composite -resize 512x512 -gravity center $2
