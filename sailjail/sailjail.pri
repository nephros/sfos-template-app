OTHER_FILES += \
    $$PWD/$${TARGET}.profile \
    $$PWD/$${TARGET}.local \

INSTALLS += sjprofile fjprofile

sjprofile.files = $$PWD/$${TARGET}.profile
sjprofile.path = $$INSTALL_ROOT/etc/sailjail/permissions

fjprofile.files = $$PWD/$${TARGET}.local
fjprofile.path = $$INSTALL_ROOT/etc/firejail

#QMAKE_EXTRA_TARGETS += strings sjts sjqm
QMAKE_EXTRA_COMPILERS += sjts
#PRE_TARGETDEPS += strings sjts sjqm

# String generator
strings.output = $$PWD/strings.cpp
strings.input = $$PWD
strings.commands = python3 $$PWD/tools/generate_translation_strings.py $$strings.input > $$strings.output
strings.CONFIG += no_link

#SJ_TRANSLATIONS = translations/$$TARGET-sailjail-permissions-en.ts \
TRANSLATIONS = translations/$$TARGET-sailjail-permissions-en.ts \
                  translations/$$TARGET-sailjail-permissions-de.ts \
                  translations/$$TARGET-sailjail-permissions-sv.ts

sjts.commands += lupdate $$PWD -ts ${QMAKE_FILE_IN}
sjts.CONFIG += no_check_exist no_link target_predeps
sjts.input = TRANSLATIONS
#sjts.input = $$strings.output
#sjts.output = TRANSLATIONS
#sjts.output = $$OUT_PWD/translations/${QMAKE_FILE_BASE}.ts
sjts.output = ${QMAKE_FILE_BASE}.ts
sjts.depends = strings

#sjqm.commands += lrelease -idbased ${QMAKE_FILE_NAME} -qm ${QMAKE_FILE_OUT}
#sjqm.CONFIG += no_check_exist no_link
#sjqm.depends = sjts
#sjqm.input = $$sjts.output
##sjqm.input = TRANSLATIONS
#sjqm.output = $$OUT_PWD/translations/${QMAKE_FILE_BASE}.qm
#
#sjqm_install.path = /usr/share/translations
#sjqm_install.files = $$sjqm.output
#sjqm_install.CONFIG += no_check_exist
#
#INSTALLS += sjqm_install

