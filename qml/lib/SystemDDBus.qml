// This file is part of AppTemplate.
//
// Copyright (c) 2025 Peter G. (nephros)
// SPDX-License-Identifier: Apache-2.0

import QtQuick 2.6
import Nemo.DBus 2.0

/*! SystemDDBus

    \brief wraps the "org.freedesktop.systemd1.Manager" interface
*/
Item { id: root
    //!  record jobs we launched, used as key-value store
    property var watchedJobs: {()}

    //! type:DBusInterface systemd dbus
    property alias dbus: manager

    //! true if we connect to the session bus
    readonly property bool isSessionBus: manager.bus == DBus.SessionBus
    //! true if we connect to the system bus
    readonly property bool isSystemBus: !isSessionBus // convenience

    //!  Updates internal state about units
    function refresh() {
        console.debug("update...");
        manager.getUnits();
    }

    /*! \class Manager
        \brief The main interface to Systemd Manager
    */
    DBusInterface { id: manager
        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        propertiesEnabled: true

        /* start handling signals only after the first run: */
        signalsEnabled: false
        //Component.onCompleted: call('Subscribe'); // get signals

        //! Signal handler
        function unitFilesChanged() {
            console.debug("dbus UnitFiles changed.");
            app.refresh();
        }

        property int nNames //! This is just defined so we get signals
        property int nFailedUnits //! This is just defined so we get signals
        //! Signal handler
        onNNamesChanged: {
            console.debug("dbus NNames changed (" + nNames + ")");
            app.refresh();
        }
        //! Signal handler
        onNFailedUnitsChanged: {
            console.debug("dbus NFailedUnits changed (" + nFailedUnits + ")");
            app.refresh();
        }

        //! Switch between DBus SystemBus and SessionBus
        function switchBus() {
            bus = (bus == DBus.SessionBus) ? DBus.SystemBus : DBus.SessionBus
        }

        //! Enable unit file \c u
        //! @param type:string u
        function enable(u) {
            //console.debug("dbus enable unit", u );
            typedCall('EnableUnitFiles', [
                { "type": "as", "value": [u] },
                { "type": "b", "value": false },
                { "type": "b", "value": false },
            ],
                function(result) { successMsg(qsTr("Enable %1").arg(u), result[1]); app.refresh() },
                function(result) { failMsg(qsTr("Enable %1").arg(u), result[1]) }
            );
        }
        function disable(u) {
            //console.debug("dbus disable unit", u );
            typedCall('DisableUnitFiles', [
                { "type": "as", "value": [u] },
                { "type": "b", "value": false },
            ],
                function(result) { successMsg(qsTr("Disable %1").arg(u), result); app.refresh() },
                function(result) { failMsg(qsTr("Disable %1").arg(u), result) }
            );
        }
        function start(u) {
            //console.debug("dbus start unit", u );
            call('StartUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); watchJob(result, qsTr("Start")); },
                function(result) { failMsg(qsTr("Start %1").arg(u), result) }
            );
        }
        function stop(u) {
            //console.debug("dbus stop unit", u );
            call('StopUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); watchJob(result, qsTr("Stop")); },
                function(result) { failMsg(qsTr("Stop %1").arg(u), result) }
            );
        }
        function restart(u) {
            //console.debug("dbus restart unit", u );
            call('RestartUnit',
                [u, "replace",],
                function(result) { console.debug("Job:", JSON.stringify(result)); watchJob(result, qsTr("Restart")); },
                function(result) { failMsg(qsTr("Restart %1").arg(u), result) }
            );
        }
        function preset(u) {
            //console.debug("dbus restart unit", u );
            call('PresetUnitFiles',
                [u, false, false,],
                function(result) { successMsg(qsTr("Preset %1").arg(u), result) },
                function(result) { failMsg(qsTr("Preset %1").arg(u), result) }
            );
        }
        function daemon_reload() {
            //console.debug("daemon reload ");
            call('Reload',
                [],
                function(result) { successMsg(qsTr("Preset %1").arg(u), result) },
                function(result) { failMsg(qsTr("Preset %1").arg(u), result) }
            );
        }

        //signal handler:
        //function jobNew(id, job, unit) {
        //    console.debug("Job added: ", id, "/", nJobs)
        //}

        //! Signal handler for finished jobs
        function jobRemoved(id, job, unit, result) {
            //! TODO: maybe we want notifications about all jobs:
            if (!watchedJobs.hasOwnProperty(job) ) { return; }
            console.debug("Job removed: ", id, "/", result, unit)
            if (result == "done") {
                successMsg(watchedJobs[job], qsTr("%2 for unit %3", "successful message, e.g. 'start for unit foo'").arg(result).arg(unit))
            } else {
                failMsg(watchedJobs[job], qsTr("%1 %2 for unit %3","failure message, e.g. 'starting failed for unit foo'").arg(watchedJobs[job]).arg(result).arg(unit))
            }
            unwatchJob(job);
            app.refresh();
        }
        //! handle the internal job queue, we only report jobs we triggered:
        // TODO: maybe we want notifications about all jobs:
        function watchJob(job, action) {
            console.debug("watching:",job);
            watchedJobs[job] = action;
        }
        //! handle the internal job queue, we only report jobs we triggered:
        function unwatchJob(job) {
            console.debug("unwatching:",job);
            if (watchedJobs.hasOwnProperty(job)) {
                delete watchedJobs[job];
            } else {
                console.warn("Trying to remove job not in list");
            }
        }

        //! @param type:string  u unit
        //! @param type:funtion cp callback
        function getUnitState(u, cb) {
            call('GetUnitFileState',
                [u],
                function(result) { cb(result) },
                function(result) { console.debug("failure: ", u, result) }
            );
        }

        /*!
           Get a list of Units and popuplate a model
        */
        function getUnits() {

            /*
            Available unit load states:
            stub loaded not-found error merged masked

            Available unit active states:
            active reloading inactive failed activating deactivating

            Available automount unit substates:
            dead waiting running failed

            Available mount unit substates:
            dead mounting mounting-done mounted remounting unmounting
            remounting-sigterm remounting-sigkill unmounting-sigterm
            unmounting-sigkill failed

            Available path unit substates:
            dead waiting running failed

            Available service unit substates:
            dead start-pre start start-post running exited reload stop
            stop-sigabrt stop-sigterm stop-sigkill stop-post final-sigterm
            final-sigkill failed auto-restart

            Available timer unit substates:
            dead waiting running elapsed failed
            */
            const filterstates = [
                    "loaded", "masked",                 //! unit load state
                    "inactive", "active", "failed",     //! unit active state
                    "dead", "waiting", "running",       //! common substates
                    "mounted", "mounting-done",         //! mount substate
                    "exited", "auto-restart",           //! service substates
                    "start-pre", "start-post",          //! service substates
                    "static",                           //! ???
                    "elapsed",                          //! timer substate
                    "listening",                        //! socket substate
                ];
            // call('ListUnits',undefined,
            typedCall('ListUnitsFiltered', { "type": "as", "value": filterstates },
                function(result) { fillModels(result); },
                function(result) { console.critical("Failed to get list of Units: ", result) }
            );
            /* start handling signals only after the first run: */
            if (!signalsEnabled) {
                console.debug("Enabling signals from systemd");
                signalsEnabled = true;
                 call('Subscribe'); //! get signals
            }
        }
    }

    /*
     ***************************
     * Popups and Notifications
     ***************************
     */
    Notification { id: smessage; isTransient: true; }
    Notification { id: message; isTransient: true; appName: Qt.application.name; appIcon: "harbour-sailord";
        property string pageName: "MainPage" // FIXME
        remoteActions: [ {
            "name": "default",
            "displayName": "Open",
            "service": listener.service,
            "path":    listener.path,
            "iface":   listener.iface,
            "method": "openPage",
            "arguments": [ pageName ]
        } ]
    }

    //! Show a notification(message)
    function popup(s) {
        smessage.previewSummary = s
        //m ? message.previewBody = m : true
        smessage.urgency = 0;
        smessage.publish();
    }
    //! Show a notification (action, message)
    function successMsg(a, m) {
        if (!config.showSuccess) return
        console.debug("success:", m)
        message.previewSummary = qsTr("%1 successful.",  "arg is an operation, such as 'restarting service X'").arg(a);
        message.summary = qsTr("Success:");
        message.body = a + ": " + m;
        message.category = "device"
        message.urgency = 0;
        message.publish();

    }
    //! Show a notification(action, message)
    function failMsg(a, m) {
        console.warn("failed:",m)
        message.previewSummary = qsTr("%1 failed.",  "arg is an operation, such as 'restarting service X'").arg(a);
        message.summary = qsTr("Failure:");
        message.body = a + ": " + m;
        message.category = "device.error";
        message.urgency = 2;
        message.publish();
    }

    /*!
       Helper function.

       Search a QML model for a certain key
       @param type:Object m model: the input model
       @param type:string k key: the role name to search
       @param type:var    v model: what to search for
    */
    function contains(m, k, v) {
        for (var i=0; i<m.count; i++) {
            if (m.get(i)[k] == v) return true
        }
        return false
    }

}

// vim: ft=javascript syntax=qml expandtab ts=4 sw=4 st=4
